/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class OperacionesRegistros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

         
       int op;
       
       Scanner entrada = new Scanner(System.in);
      do{
       System.out.println(".:: OPERACIONES CON REGISTROS ::.");
       System.out.println("1.- REGISTRO AX");
       System.out.println("2.- REGISTRO BX");
       System.out.println("3.- REGISTRO CX");
       System.out.println("4.- REGISTRO DX");
       System.out.println("5.- SALIR");
       op=entrada.nextInt();
       
       switch(op) 
       {
           
        case 1:
        System.out.println(".:: Registro AX ::.");
        int elemento, n=8;    
       String[] arregloax1;           
       String[] arregloax2;   
       int[] arregloax3;     
        
       arregloax1 = new String [ n ];
       arregloax2 = new String [ n ];
       arregloax3 = new int [ n ];
       
       System.out.println("Introduce los bits del registro AH");
         for (elemento = 0; elemento < n; elemento++)
        {
     
        arregloax1[elemento] = entrada.next();
    
        }
        System.out.println("Introduce los bits del registro AL");
         for (elemento = 0; elemento < n; elemento++)
        {
     
        arregloax2[elemento] = entrada.next();
       
        }
        System.out.println("Suma resultante del registro AX"); 
        for (elemento = 0; elemento < n; elemento++)
        {   
            arregloax3[elemento]= Integer.parseInt(arregloax1[elemento],2) + Integer.parseInt(arregloax2[elemento],2);
            String resultado = Integer.toString(arregloax3[elemento], 2);
            System.out.print( resultado ); 
        }  
        break;
               
      case 2:
       System.out.println(".:: Registro BX ::.");
       int elemento1, n1=8;    
       String[] arreglobx1;           
       String[] arreglobx2;   
       int[] arreglobx3;     
        
       arreglobx1 = new String [ n1 ];
       arreglobx2 = new String [ n1 ];
       arreglobx3 = new int [ n1 ];
       
       System.out.println("Introduce los bits del registro BH");
         for (elemento1 = 0; elemento1 < n1; elemento1++)
        {
     
        arreglobx1[elemento1] = entrada.next();
    
        }
        System.out.println("Introduce los bits del registro BL");
         for (elemento1 = 0; elemento1 < n1; elemento1++)
        {
     
        arreglobx2[elemento1] = entrada.next();
       
        }
        System.out.println("Suma resultante del registro BX"); 
        for (elemento1 = 0; elemento1 < n1; elemento1++)
        {   
            arreglobx3[elemento1]= Integer.parseInt(arreglobx1[elemento1],2) + Integer.parseInt(arreglobx2[elemento1],2);
            String resultado = Integer.toString(arreglobx3[elemento1], 2);
            System.out.print( resultado ); 
        }  
        break;
       
      case 3:
        
       System.out.println(".:: Registro CX ::.");
       int elemento2, n2=8;    
       String[] arreglocx1;           
       String[] arreglocx2;   
       int[] arreglocx3;     
        
       arreglocx1 = new String [ n2 ];
       arreglocx2 = new String [ n2 ];
       arreglocx3 = new int [ n2 ];
       
       System.out.println("Introduce los bits del registro CH");
         for (elemento2 = 0; elemento2 < n2; elemento2++)
        {
     
        arreglocx1[elemento2] = entrada.next();
    
        }
        System.out.println("Introduce los bits del registro CL");
         for (elemento2 = 0; elemento2 < n2; elemento2++)
        {
     
        arreglocx2[elemento2] = entrada.next();
       
        }
        System.out.println("Suma resultante del registro CX"); 
        for (elemento2 = 0; elemento2 < n2; elemento2++)
        {   
            arreglocx3[elemento2]= Integer.parseInt(arreglocx1[elemento2],2) + Integer.parseInt(arreglocx2[elemento2],2);
            String resultado = Integer.toString(arreglocx3[elemento2], 2);
            System.out.print( resultado ); 
        }   
        break;
      
      case 4:
       
       System.out.println(".:: Registro DX ::.");
       int elemento3, n3=8;    
       String[] arreglodx1;           
       String[] arreglodx2;   
       int[] arreglodx3;     
        
       arreglodx1 = new String [ n3 ];
       arreglodx2 = new String [ n3 ];
       arreglodx3 = new int [ n3 ];
       
       System.out.println("Introduce los bits del registro DH");
         for (elemento3 = 0; elemento3 < n3; elemento3++)
        {
     
        arreglodx1[elemento3] = entrada.next();
    
        }
        System.out.println("Introduce los bits del registro DL");
         for (elemento3 = 0; elemento3 < n3; elemento3++)
        {
     
        arreglodx2[elemento3] = entrada.next();
       
        }
        System.out.println("Suma resultante del registro DX"); 
        for (elemento3 = 0; elemento3 < n3; elemento3++)
        {   
            arreglodx3[elemento3]= Integer.parseInt(arreglodx1[elemento3],2) + Integer.parseInt(arreglodx2[elemento3],2);
            String resultado = Integer.toString(arreglodx3[elemento3], 2);
            System.out.print( resultado ); 
        }                                                                                                
       }
      }while(op != 5);
    }
}
